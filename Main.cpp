#include <Windows.h>
#include <iostream>
#include "Helper.h"

using namespace std;


bool Check_Exit(string Command)
{
	if (Command.compare("EXIT") == 0)
	{
		return(true);
	}
	if (Command.compare("Exit") == 0)
	{
		return(true);
	}
	if (Command.compare("EXIT()") == 0)
	{
		return(true);
	}
	if (Command.compare("exit()") == 0)
	{
		return(true);
	}
	if (Command.compare("exit") == 0)
	{
		return(true);
	}
	if (Command.compare("shutdown") == 0)
	{
		return(true);
	}
	return(false);
}

string PWD_Handler()
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	string::size_type pos = string(buffer).find_last_of("\\/");
	return string(buffer).substr(0, pos);
}

bool Create_File(string path)
{
	HANDLE hFile;

	hFile = CreateFile(path.c_str(),GENERIC_WRITE,0,NULL,CREATE_NEW,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		return(false);
	}
	if (hFile != INVALID_HANDLE_VALUE) 
	{
		::CloseHandle(hFile);
		hFile = INVALID_HANDLE_VALUE;
	}
	return(true);
}

bool LS_Command()
{

	return(false);
}

string Get_Command(string Command)
{
	string res;
	if (Command[0] == 'P' && Command[1] == 'W' && Command[2] == 'D' || Command[0] == 'p' && Command[1] == 'w' && Command[2] == 'd')
	{
		res = "PWD";
	}
	if (Command[0] == 'P' && Command[1] == 'w' && Command[2] == 'd')
	{
		res = "PWD";
	}
	if (Command[0] == 'C' && Command[1] == 'D' || Command[0] == 'c' && Command[1] == 'd' || Command[0] == 'C' && Command[1] == 'd')
	{
		res = "CD";
	}
	if (Command[0] == 'C' && Command[1] == 'r' && Command[2] == 'e' && Command[3] == 'a' && Command[4] == 't' && Command[5] == 'e')
	{
		res = "CREATE";
	}
	if (Command[0] == 'C' && Command[1] == 'R' && Command[2] == 'E' && Command[3] == 'A' && Command[4] == 'T' && Command[5] == 'E')
	{
		res = "CREATE";
	}
	if (Command[0] == 'c' && Command[1] == 'r' && Command[2] == 'e' && Command[3] == 'a' && Command[4] == 't' && Command[5] == 'e')
	{
		res = "CREATE";
	}
	if (Command[0] == 'L' && Command[1] == 'S' || Command[0] == 'l' && Command[1] == 's' || Command[0] == 'L' && Command[1] == 's')
	{
		res = "LS";
	}
	if (Command[0] == 'S' && Command[1] == 'e' && Command[2] == 'c' && Command[3] == 'r' && Command[4] == 'e' && Command[5] == 't')
	{
		res = "SECRET";
	}
	if (Command[0] == 'S' && Command[1] == 'E' && Command[2] == 'C' && Command[3] == 'R' && Command[4] == 'E' && Command[5] == 'T')
	{
		res = "SECRET";
	}
	if (Command[0] == 's' && Command[1] == 'e' && Command[2] == 'c' && Command[3] == 'r' && Command[4] == 'e' && Command[5] == 't')
	{
		res = "SECRET";
	}
	if (Command[0] == 'C' && Command[1] == 'l' && Command[2] == 'e' && Command[3] == 'a' && Command[4] == 'r')
	{
		res = "CLEAR";
	}
	if (Command[0] == 'C' && Command[1] == 'L' && Command[2] == 'E' && Command[3] == 'A' && Command[4] == 'R')
	{
		res = "CLEAR";
	}
	if (Command[0] == 'c' && Command[1] == 'l' && Command[2] == 'e' && Command[3] == 'a' && Command[4] == 'r')
	{
		res = "CLEAR";
	}
	return res;
}

int main()
{
	string Command;
	char Comnand_Part[100];
	string Command_Check;
	string Output;
	string path;
	string Inner_Command;
	bool Output_Flag = false;
	bool check = true;
	cout << ">>";
	getline(cin,Command);
	Helper::trim(Command);
	Inner_Command = Get_Command(Command);
	while(!Check_Exit(Command))
	{
		Output_Flag = false;
		if (Inner_Command.compare("PWD") == 0)
		{
			Output = PWD_Handler();
			Output_Flag = true;
		}
		if (Inner_Command.compare("CD") == 0)
		{
			cout << "cd" << endl;
		}
		if (Inner_Command.compare("CREATE") == 0)
		{
			path = string(Command.substr(7));
			cout << path << endl;
			check = Create_File(path);
			if (check == false)
			{
				cout << "Error - File Not Created!!!" << endl;
			}
			else
			{
				cout << "Success- File Created!!!" << endl;
			}
		}
		if (Inner_Command.compare("LS") == 0)
		{

		}
		if (Inner_Command.compare("SECRET") == 0)
		{
			cout << "" << endl;
		}
		if (Inner_Command.compare("CLEAR") == 0)
		{
			system("CLS");
		}
		if (Output_Flag)
		{
			cout << Output << endl;
		}
		cout << ">>";
		getline(cin, Command);
		Helper::trim(Command);
		Inner_Command = Get_Command(Command);
	}
	cout << "Shutdown..... Goodbye!!!" << endl;
	system("PAUSE");
	return(0);
}

